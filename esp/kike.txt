Pick up from your hotel in Lima and transfer to the bus station. 
Bus tickets from Lima to Ica (Cruzero Bus Standard Service). 
Pick up from the station upon your arrival in Ica and transfer to the selected hotel. 
City Tour of Ica. 
Dune Buggy Excursion around Huacachina. 
01 night accommodation in Ica or Huacachina. 
Day 2�

Private transportation to the airport of Nazca. 
Visi to the Palpa & Nazca Lines by land. 
Guided visit to the Maria Reiche Museum. 
Flight over the Nazca Lines. 
Airport taxes. 
01 night accommodation in Ica or Huacachina. 
Day 3�

Private transportation to the Paracas Bay. 
Boat excursion to the Ballestas Islands. 
Guided visit to the Paracas Reserve. 
Transfer to the bus station in Paracas. 
Bus tickets from Paracas to Lima (Cruzero Bus Standard Service). 
Pick up from the bus station upon your arrival in Lima and transfer to your hotel. 