WPGgallery = new Object();
WPGgallery.name = "Pampas%20Galeras%20Reserve";
WPGgallery.photographer = "";
WPGgallery.contact = "";
WPGgallery.email = "";
WPGgallery.date = ""

WPGgallery.colors = new Object();
WPGgallery.colors.background = "#000000";
WPGgallery.colors.banner = "#363636";
WPGgallery.colors.text = "#FFFFFF";
WPGgallery.colors.link = "#D57800";
WPGgallery.colors.alink = "#FF0000";
WPGgallery.colors.vlink = "#744201";

gPhotos = new Array();
gPhotos[0] = new Object();
gPhotos[0].filename = "photo01.jpg";
gPhotos[0].ImageWidth = 600;
gPhotos[0].ImageHeight = 450;
gPhotos[0].ThumbWidth = 75;
gPhotos[0].ThumbHeight = 56;
gPhotos[0].meta = new Object();

gPhotos[1] = new Object();
gPhotos[1].filename = "photo02.jpg";
gPhotos[1].ImageWidth = 600;
gPhotos[1].ImageHeight = 450;
gPhotos[1].ThumbWidth = 75;
gPhotos[1].ThumbHeight = 56;
gPhotos[1].meta = new Object();

gPhotos[2] = new Object();
gPhotos[2].filename = "photo03.jpg";
gPhotos[2].ImageWidth = 600;
gPhotos[2].ImageHeight = 450;
gPhotos[2].ThumbWidth = 75;
gPhotos[2].ThumbHeight = 56;
gPhotos[2].meta = new Object();

gPhotos[3] = new Object();
gPhotos[3].filename = "photo04.jpg";
gPhotos[3].ImageWidth = 600;
gPhotos[3].ImageHeight = 450;
gPhotos[3].ThumbWidth = 75;
gPhotos[3].ThumbHeight = 56;
gPhotos[3].meta = new Object();

gPhotos[4] = new Object();
gPhotos[4].filename = "photo05.jpg";
gPhotos[4].ImageWidth = 600;
gPhotos[4].ImageHeight = 450;
gPhotos[4].ThumbWidth = 75;
gPhotos[4].ThumbHeight = 56;
gPhotos[4].meta = new Object();

gPhotos[5] = new Object();
gPhotos[5].filename = "photo06.jpg";
gPhotos[5].ImageWidth = 600;
gPhotos[5].ImageHeight = 450;
gPhotos[5].ThumbWidth = 75;
gPhotos[5].ThumbHeight = 56;
gPhotos[5].meta = new Object();

gPhotos[6] = new Object();
gPhotos[6].filename = "photo07.jpg";
gPhotos[6].ImageWidth = 600;
gPhotos[6].ImageHeight = 450;
gPhotos[6].ThumbWidth = 75;
gPhotos[6].ThumbHeight = 56;
gPhotos[6].meta = new Object();

gPhotos[7] = new Object();
gPhotos[7].filename = "photo08.jpg";
gPhotos[7].ImageWidth = 600;
gPhotos[7].ImageHeight = 450;
gPhotos[7].ThumbWidth = 75;
gPhotos[7].ThumbHeight = 56;
gPhotos[7].meta = new Object();

gPhotos[8] = new Object();
gPhotos[8].filename = "photo09.jpg";
gPhotos[8].ImageWidth = 600;
gPhotos[8].ImageHeight = 450;
gPhotos[8].ThumbWidth = 75;
gPhotos[8].ThumbHeight = 56;
gPhotos[8].meta = new Object();

gPhotos[9] = new Object();
gPhotos[9].filename = "photo10.jpg";
gPhotos[9].ImageWidth = 600;
gPhotos[9].ImageHeight = 450;
gPhotos[9].ThumbWidth = 75;
gPhotos[9].ThumbHeight = 56;
gPhotos[9].meta = new Object();

gPhotos[10] = new Object();
gPhotos[10].filename = "photo11.jpg";
gPhotos[10].ImageWidth = 600;
gPhotos[10].ImageHeight = 450;
gPhotos[10].ThumbWidth = 75;
gPhotos[10].ThumbHeight = 56;
gPhotos[10].meta = new Object();

gPhotos[11] = new Object();
gPhotos[11].filename = "photo12.jpg";
gPhotos[11].ImageWidth = 600;
gPhotos[11].ImageHeight = 450;
gPhotos[11].ThumbWidth = 75;
gPhotos[11].ThumbHeight = 56;
gPhotos[11].meta = new Object();

gPhotos[12] = new Object();
gPhotos[12].filename = "photo13.jpg";
gPhotos[12].ImageWidth = 600;
gPhotos[12].ImageHeight = 450;
gPhotos[12].ThumbWidth = 75;
gPhotos[12].ThumbHeight = 56;
gPhotos[12].meta = new Object();

gPhotos[13] = new Object();
gPhotos[13].filename = "photo14.jpg";
gPhotos[13].ImageWidth = 600;
gPhotos[13].ImageHeight = 450;
gPhotos[13].ThumbWidth = 75;
gPhotos[13].ThumbHeight = 56;
gPhotos[13].meta = new Object();

gPhotos[14] = new Object();
gPhotos[14].filename = "photo15.jpg";
gPhotos[14].ImageWidth = 600;
gPhotos[14].ImageHeight = 450;
gPhotos[14].ThumbWidth = 75;
gPhotos[14].ThumbHeight = 56;
gPhotos[14].meta = new Object();

gPhotos[15] = new Object();
gPhotos[15].filename = "photo16.jpg";
gPhotos[15].ImageWidth = 600;
gPhotos[15].ImageHeight = 450;
gPhotos[15].ThumbWidth = 75;
gPhotos[15].ThumbHeight = 56;
gPhotos[15].meta = new Object();

gPhotos[16] = new Object();
gPhotos[16].filename = "photo17.jpg";
gPhotos[16].ImageWidth = 600;
gPhotos[16].ImageHeight = 450;
gPhotos[16].ThumbWidth = 75;
gPhotos[16].ThumbHeight = 56;
gPhotos[16].meta = new Object();

gPhotos[17] = new Object();
gPhotos[17].filename = "photo18.jpg";
gPhotos[17].ImageWidth = 600;
gPhotos[17].ImageHeight = 450;
gPhotos[17].ThumbWidth = 75;
gPhotos[17].ThumbHeight = 56;
gPhotos[17].meta = new Object();

gPhotos[18] = new Object();
gPhotos[18].filename = "photo19.jpg";
gPhotos[18].ImageWidth = 600;
gPhotos[18].ImageHeight = 450;
gPhotos[18].ThumbWidth = 75;
gPhotos[18].ThumbHeight = 56;
gPhotos[18].meta = new Object();

gPhotos[19] = new Object();
gPhotos[19].filename = "photo20.jpg";
gPhotos[19].ImageWidth = 600;
gPhotos[19].ImageHeight = 450;
gPhotos[19].ThumbWidth = 75;
gPhotos[19].ThumbHeight = 56;
gPhotos[19].meta = new Object();

gPhotos[20] = new Object();
gPhotos[20].filename = "photo21.jpg";
gPhotos[20].ImageWidth = 600;
gPhotos[20].ImageHeight = 450;
gPhotos[20].ThumbWidth = 75;
gPhotos[20].ThumbHeight = 56;
gPhotos[20].meta = new Object();

